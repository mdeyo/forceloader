<p align="center">
    <img width="600" src=".gitlab/ForceLoader.png" title="Project logo"><br />
</p>

# ForceLoader

ForceLoader is an native OS alternative to Salesforce's DataLoader for Windows and MacOS.

## ✨Features

* 🥢 No Java runtime dependency.
* 🎨 Native to your OS for great performance.
* 🖖 Backwards compatible.
* ✔️ Comes with a CLI.

## 🎈 Status

This project is currently under heavy development and in a pre-alpha state. Nothing works at the moment.

