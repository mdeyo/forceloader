const std = @import("std");
const sqlite3 = @import("c/sqlite3.zig");
const testing = std.testing;

fn initializeDatabase() c_int {
    const result = sqlite3.sqlite3_initialize();
    defer _ = sqlite3.sqlite3_shutdown();
    return result;
}

export fn add(a: i32, b: i32) i32 {
    return a + b;
}

test "basic add functionality" {
    try testing.expect(add(3, 7) == 10);
}

test "db init" {
    try testing.expectEqual(initializeDatabase(), 0);
}
