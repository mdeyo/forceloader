# Roadmap

This file provides an overview of the direction this project is heading.

✅ = Feature is complete and ready to use.
🔧 = Feature is being developed and may be partially available.
❌ = Development of this has not been started.

## v0.1.0

- ❌ Basic Salesforce Authentication
    - ❌ Password Authentication
    - X  Saved Credentials with Native Password Store
- X  Core Functions
    - X  Bulk API
        - X  Query/Export as CSV
        - X  Insert using CSV
        - X  Update using CSV
        - X  Upsert using CSV
        - X  Delete using CSV
- ❌ Simple YAML Configuration
    - ❌ Default File Save Location
    - ❌ Default Credentials
- X  Simple Job History
